<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
		<link href="/css/app.css" rel="stylesheet">
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script>
			jQuery(document).ready(function() {
			  
                  
		/*		$('table tr th:nth-child(6) button').css('display','none');	
				$('table tr th:nth-child(6) span').css('display','block');	
				$('tr td:nth-child(6)').on('mousedown',function(){
					// name attribute contains id of product
					
					$(this).on('mouseup',function(){
						if($('table tr td:contains("Yes")').length > 0)	{
							$('table tr th:nth-child(6) button').css('display','block');
							$('table tr th:nth-child(6) span').css('display','none');
							
						}
						if($('table tr td:contains("Yes")').length == 0){
								$('table tr th:nth-child(6) span').css('display','block').text('Want to Edit?');
								$('table tr th:nth-child(6) button').css('display','none');
						}
					});
					
					
					if($(this).text() == 'No, but Click for Yes')				
						$(this).text('Yes, but Click for No');
					else if($(this).text() == 'Yes, but Click for No')
						$(this).text('No, but Click for Yes');
			
				});
				$('form#edits button').on('click',function(){				        
                        $('table tr td:contains("Yes, but Click")').each(function(){
							 var id = $(this).attr("name");
							 $('form#edits')
							   .append('<input type="hidden" name="id[]" value="'+ id +'">');
						 });
						 
						 $('form#edits').submit();
					
					});
					
				$('form#deleteItem button').on('click',function(){				        
                         var name = $('form input#name').val()
						 $('form#deleteItem')
						   .append('<input type="hidden" name="name" value="'+ name +'">');
					
						 
						 $('form#deleteItem').submit();
					
					});	*/
			
			});
		</script>
      
    </head>
    <body>      

		<h2 style="position:relative;width:auto;margin-left:45%">Transcript</h2>
	   
        <div class="content">
				
				<!!--@if (count($courses) > 0) -->
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="panel panel-success">
								<div class="panel-heading">Courses</div>						
									<!-- Table -->
									<table class="table table-bordered table-striped">
										<tr>
											<th>Student Name</th>
											<th>GradeLevel</th>								
											<th>Course Name</th>								
											<th>Grade</th>
										</tr>
									  @foreach($courses as $course)
										<tr>
											<td>{{ $course->studentName}}</td>
											<td>{{ $course->gradeLevel }}</td>
											<td>{{ $course->courseName }}</td>
											<td>{{ $course->finalGrade }}</td>
										</tr>
									  @endforeach
									</table>				
								</div>
							</div>
						</div>
					</div>
				<!-- @endif -->
			
        </div>
		
	<!--<meta name="_token" content="{!! csrf_token() !!}" /> -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    </body>
</html>

