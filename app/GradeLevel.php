<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeLevel extends Model
{
    protected $table = 'gradeLevels';
	
	/**
     * Get all of the Courses for the Grade Level.
     */
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
	
	
	/**
     * Get Student the Grade Level Belongs to
     */
	 public function student()
    {
        return $this->belongsTo(Student::class);
    }
	
}

	
	